import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ServicesService } from '../service/services.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  profile: any = {};
  constructor(private service: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getProfile();
  }

  getProfile(): void {
    this.service.getProfiles()
      .subscribe(profile => this.profile = profile);
  }
}
