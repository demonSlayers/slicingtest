import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ServicesService } from '../service/services.service';
import { Test } from '../interface';

@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit {
  channels: Test[] = [];

  constructor(private service: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getChannel();
  }

  getChannel(): void {
    this.service.getChannels()
      .subscribe(channels => this.channels = channels);
  }

}

