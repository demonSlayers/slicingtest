import { Component, OnInit } from '@angular/core';
import { speedDialFabAnimations } from './animations';

@Component({
  selector: 'app-animations',
  templateUrl: './animations.component.html',
  styleUrls: ['./animations.component.scss'],
  animations: speedDialFabAnimations
})
export class AnimationsComponent implements OnInit {
  fabButtons = [
    {
      icon: 'edit'
    },
    {
      icon: 'delete'
    },
    {
      icon: 'close'
    }
  ];
  buttons = [];
  fabTogglerState = 'inactive';
  constructor() { }

  ngOnInit(): void {
  }

  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }
}
