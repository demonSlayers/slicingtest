import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ServicesService } from '../service/services.service';
import { Test } from '../interface';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {
  documents: Test[] = [];
  constructor(private service: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getDocument();
  }

  getDocument(): void {
    this.service.getDocuments()
      .subscribe(documents => this.documents = documents);
  }
}
