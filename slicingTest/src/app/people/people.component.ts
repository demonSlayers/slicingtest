import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ServicesService } from '../service/services.service';
import { Test } from '../interface';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {
  peoples: Test[] = [];

  constructor(private service: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getPeople();
  }

  getPeople(): void {
    this.service.getPeoples()
      .subscribe(peoples => this.peoples = peoples);
  }

}
