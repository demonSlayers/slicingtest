export interface Test {
    userId: number;
    id: number;
    name: string;
    title: string;
    url: string;
    pUrl: string;
    rows: number;
    cols: number;
}
