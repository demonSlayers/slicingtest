import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentsComponent } from './documents/documents.component';
import { ContentsComponent } from './contents/contents.component';
import { PeopleComponent } from './people/people.component';
import { ActivitiesComponent } from './activities/activities.component';
import { ChannelsComponent } from './channels/channels.component';
import { VideosComponent } from './videos/videos.component';

const routes: Routes = [
  { path: 'documents', component: DocumentsComponent},
  { path: 'people', component: PeopleComponent},
  { path: 'activities', component: ActivitiesComponent},
  { path: 'channels', component: ChannelsComponent},
  { path: 'videos', component: VideosComponent},
  { path: '', component: ContentsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
