import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Test } from '../interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  private serviceUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getDocuments(): Observable<Test[]> {
    return this.http.get<Test[]>(`${this.serviceUrl}/documents`);
  }

  getVideos(): Observable<Test[]> {
    return this.http.get<Test[]>(`${this.serviceUrl}/videos`);
  }

  getPeoples(): Observable<Test[]> {
    return this.http.get<Test[]>(`${this.serviceUrl}/people`);
  }

  getChannels(): Observable<Test[]> {
    return this.http.get<Test[]>(`${this.serviceUrl}/channels`);
  }

  getActivities(): Observable<Test[]> {
    return this.http.get<Test[]>(`${this.serviceUrl}/activities`);
  }

  getProfiles(): Observable<Test[]> {
    return this.http.get<Test[]>(`${this.serviceUrl}/people/1`);
  }
}
