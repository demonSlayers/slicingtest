import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ServicesService } from '../service/services.service';
import { Test } from '../interface';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {
  activities: Test[] = [];
  constructor(private service: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getActivity();
  }

  getActivity(): void {
    this.service.getActivities()
      .subscribe(activities => this.activities = activities);
  }

}
